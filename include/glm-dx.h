#pragma once

#define GLM_FORCE_SWIZZLE
#define GLM_FORCE_INLINE
#define GLM_FORCE_INTRINSICS
#define GLM_FORCE_LEFT_HANDED
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#pragma warning(push, 0)
#include "../glm/glm/glm.hpp"
#pragma warning(pop)
