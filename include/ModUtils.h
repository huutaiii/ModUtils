#pragma once

/*
Configurable macros:
    MODUTILS_MACROS (bool, default = 0): Enable definition of utility macros
    MODUTILS_MACROS_PADDING (bool, default = 1): Enable definition of struct padding macros (requires MODUTILS_MACROS != 0)
    MODUTILS_MACROS_ASSERT (bool, default = 1): Assertion macros (requires MODUTILS_MACROS)
    MODUTILS_SRC_ROOT (string): Source directory (requires MODUTILS_MACROS)
*/

#ifdef _MSVC_LANG
static_assert(/*__cplusplus >= 202002L ||*/ _MSVC_LANG >= 202002L);
#else
static_assert(__cplusplus >= 202002L);
#endif

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <mutex>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <format>
#include <filesystem>
#include <algorithm>
#include <atomic>
#include <iterator>
#include <regex>

#include <windows.h>
#include <psapi.h>

#ifndef MODUTILS_MACROS
#define MODUTILS_MACROS 0
#endif
#ifndef MODUTILS_MACROS_PADDING
#define MODUTILS_MACROS_PADDING 1
#endif
#ifndef MODUTILS_MACROS_ASSERT
#define MODUTILS_MACROS_ASSERT 1
#endif

#if MODUTILS_MACROS

    #if MODUTILS_MACROS_PADDING
        #define _CATIMPL(a, b) a##b
        #define CAT(a, b) _CATIMPL(a, b)
        #define SEQ(pref) CAT(pref, __COUNTER__)
        #define PAD(size) char SEQ(_padding)[size];
    #endif

    #define _WTEXT_IMPL(s) L##s
    #define WTEXT(s) _WTEXT_IMPL(s)
    #define LOG (MODUTILS_NAMESPACE::ULog::Get())
    #define LOG_INFO (MODUTILS_NAMESPACE::ULog::UMessage(MODUTILS_NAMESPACE::ULog::EItemType::LOG_TYPE_INFO))
    // #define LOG_DEBUG (MODUTILS_NAMESPACE::ULog::UMessage(MODUTILS_NAMESPACE::ULog::EItemType::LOG_TYPE_DEBUG))
    #define LOG_WARNING (MODUTILS_NAMESPACE::ULog::UMessage(MODUTILS_NAMESPACE::ULog::EItemType::LOG_TYPE_WARNING))
    #define LOG_ERROR (MODUTILS_NAMESPACE::ULog::UMessage(MODUTILS_NAMESPACE::ULog::EItemType::LOG_TYPE_ERROR))
    #define LOG_PLAIN (MODUTILS_NAMESPACE::ULog::UMessage(MODUTILS_NAMESPACE::ULog::EItemType::LOG_TYPE_PLAIN))

    #define _QUOTE(s) #s
    #define QUOTE(s) _QUOTE(s)
    #define FUNCTION_NAME __FUNCTION__
    #ifdef MODUTILS_SRC_ROOT
    #define MODUTILS_CURRENT_SRC_FILE std::filesystem::relative(__FILE__, MODUTILS_SRC_ROOT).string()
    #else
    #define MODUTILS_CURRENT_SRC_FILE std::filesystem::path(__FILE__).filename().string()
    #endif
    #define LOG_CONTEXT (std::string("[") + MODUTILS_CURRENT_SRC_FILE + std::string("(" QUOTE(__LINE__) "): " FUNCTION_NAME) + std::string("] "))

    #if defined(NDEBUG)
    #define MODUTILS_USE_DEBUG_LOG 0
    #else
    #define MODUTILS_USE_DEBUG_LOG 1
    #endif

    #define LOG_DEBUG if (!MODUTILS_USE_DEBUG_LOG) {} else (MODUTILS_NAMESPACE::ULog::UMessage(MODUTILS_NAMESPACE::ULog::EItemType::LOG_TYPE_DEBUG))

    #define EXEC_ONCE(...)              \
        {                               \
            static bool __First = true; \
            if (__First)                \
            {                           \
                __First = false;        \
                __VA_ARGS__;            \
            }                           \
        }

    #ifdef NDEBUG
    #define EXEC_DEBUG(...) {}
    #else
    #define EXEC_DEBUG(...) { __VA_ARGS__; }
    #endif

    #if MODUTILS_MACROS_ASSERT
        #ifdef NDEBUG
        #define MODUTILS_CRT_ASSERT(exp, msg) { ((void)0); }
        #else
        #define MODUTILS_CRT_ASSERT(expression, message) { ((void)((!!(expression)) || (_wassert(_CRT_WIDE(message), _CRT_WIDE(__FILE__), (unsigned int)(__LINE__)), 0))); }
        #endif

        #define VERIFY_HRESULT(exp)                                                                                              \
            {                                                                                                                    \
                HRESULT __result__ = exp;                                                                                        \
                if (!SUCCEEDED(__result__))                                                                                      \
                {                                                                                                                \
                    EXEC_ONCE(LOG_ERROR << LOG_CONTEXT << "Expression (" #exp ") returned an error: " << std::hex << __result__) \
                }                                                                                                                \
                MODUTILS_CRT_ASSERT(SUCCEEDED(__result__), "SUCCEEDED(" WTEXT(#exp) ")");                                        \
            }

        #define ASSERT(exp)                                                              \
            {                                                                            \
                auto __result__ = exp;                                                   \
                if (!__result__)                                                         \
                {                                                                        \
                    EXEC_ONCE(LOG_ERROR << LOG_CONTEXT << "Assertion failed (" #exp ")") \
                }                                                                        \
                MODUTILS_CRT_ASSERT(__result__, #exp);                                   \
            }
    #endif // if MODUTILS_MACROS_ASSERT

#endif // if MODUTILS_MACROS

#if !MODUTILS_GLOBAL_NAMESPACE
#ifndef MODUTILS_NAMESPACE
#define MODUTILS_NAMESPACE ModUtils
#endif
#define MODUTILS_NAMESPACE_BEGIN namespace MODUTILS_NAMESPACE {
#define MODUTILS_NAMESPACE_END }
#else
#define MODUTILS_NAMESPACE
#define MODUTILS_NAMESPACE_BEGIN
#define MODUTILS_NAMESPACE_END
#endif

MODUTILS_NAMESPACE_BEGIN

std::string ConvertUTF8(std::wstring inWStr);
class ULog
{
public: 
    enum class EItemType
    {
        LOG_TYPE_INFO,
        LOG_TYPE_DEBUG,
        LOG_TYPE_WARNING,
        LOG_TYPE_ERROR,
        LOG_TYPE_PLAIN,
    };

    typedef void FLogWriteCallback(const char* pData, size_t size);

protected:
    FILE* file = nullptr;
    std::mutex file_mtx;
    std::mutex fmt_mtx;

    inline ULog(const ULog&) = delete;
    inline ULog(const ULog&&) = delete;

    inline ULog(const std::filesystem::path &path) {
    }

    inline ~ULog() {
        //if (file)
        //{
        //    fclose(file);
        //    file = nullptr;
        //}
    }

#ifdef NDEBUG
    static constexpr bool IS_DEBUG = false;
#else
    static constexpr bool IS_DEBUG = true;
#endif

public:

    static std::filesystem::path FileName;
    static std::string ModuleName;
    static bool bShowTime;
    static bool bWriteToFile;
    static bool bOutputToStdOut;
    static bool bOutputToDebugConsole;
    inline static FLogWriteCallback* Callback = nullptr;

    inline static ULog& Get()
    {
        static ULog instance(FileName.c_str());

        return instance;
    }

    inline void ClearFile()
    {
        if (FileName.empty())
        {
            return;
        }

        static std::unordered_set<std::filesystem::path> clearedFiles{};
        if (clearedFiles.contains(FileName))
        {
            return;
        }

        std::lock_guard<std::mutex> lock(file_mtx);
        std::ofstream file(FileName, std::ios_base::trunc | std::ios_base::out);
        file.close();

        clearedFiles.insert(FileName);
    }

    inline void println(const char* fmt, va_list args)
    {
        if (bWriteToFile || Callback)
        {
            std::string timestamp = {};
            if (bShowTime)
            {
                SYSTEMTIME time;
                GetLocalTime(&time);
                timestamp = std::format("{:04}-{:02}-{:02} {:02}:{:02}:{:02}.{:03} - ", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);
            }
            std::string fmtString = std::format("{}{}", timestamp, fmt);
            size_t bufferSize = vsnprintf(NULL, 0, fmtString.c_str(), args);
            std::string logString(bufferSize, '\0');
            vsnprintf(logString.data(), logString.size()+1, fmtString.c_str(), args);

            if (bWriteToFile)
            {
                ClearFile();
                std::lock_guard lock(ULog::Get().file_mtx);
                std::fstream file(FileName, std::ios_base::app | std::ios_base::binary);
                file << logString << "\n";
                file.close();
            }
            if (Callback)
            {
                Callback(logString.c_str(), bufferSize);
            }
        }

        if (bOutputToStdOut || bOutputToDebugConsole)
        {
            std::string fmtString = std::format("[{}] {}\n", ModuleName, fmt);
            size_t bufferSize = vsnprintf(NULL, 0, fmtString.c_str(), args);
            std::string logString(bufferSize, '\0');
            vsnprintf(logString.data(), logString.size()+1, fmtString.c_str(), args);
            if (bOutputToStdOut)
            {
                WriteConsoleA(GetStdHandle(STD_OUTPUT_HANDLE), logString.c_str(), bufferSize, nullptr, nullptr);
            }
            if (bOutputToDebugConsole)
            {
                OutputDebugStringA(logString.c_str());
            }
        }
    }

    inline void println(std::string fmt, ...)
    {
        va_list args;
        va_start(args, fmt);
        println(fmt.c_str(), args);
        va_end(args);
    }

    inline void eprintln(std::string fmt, ...)
    {
        std::string efmt = "[ERROR] " + (fmt);
        va_list args;
        va_start(args, fmt);
        println(efmt.c_str(), args);
        va_end(args);
    }

#ifdef _DEBUG
    inline void dprintln(std::string fmt, ...)
    {
        std::string dfmt = "[DEBUG] " + (fmt);
        va_list args;
        va_start(args, fmt);
        println(dfmt.c_str(), args);
        va_end(args);
    }
#else
    inline void dprintln(std::string fmt, ...) {}
#endif

    class UMessage
    {
        // TODO? switch to regular stringstream and convert wide char strings to utf-8
        std::wstringstream MsgStream = std::wstringstream();
        EItemType Type;

    public:
        UMessage(EItemType type = EItemType::LOG_TYPE_INFO) : Type(type) {}

        ~UMessage()
        {
            if (Type == EItemType::LOG_TYPE_DEBUG && !IS_DEBUG)
            {
                return;
            }
            std::wstring line;
            if (Type != EItemType::LOG_TYPE_PLAIN)
            {
                std::wstring timestamp{};
                if (bShowTime)
                {
                    SYSTEMTIME time;
                    GetLocalTime(&time);
                    timestamp = std::format(L"{:04}-{:02}-{:02} {:02}:{:02}:{:02}.{:03} - ", time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond, time.wMilliseconds);
                }

                std::wstring tag = L"";
                switch (Type)
                {
                default:
                case EItemType::LOG_TYPE_INFO:
                    break;
                case EItemType::LOG_TYPE_DEBUG:
                    tag = L"[DEBUG] ";
                    break;
                case EItemType::LOG_TYPE_WARNING:
                    tag = L"[WARNING] ";
                    break;
                case EItemType::LOG_TYPE_ERROR:
                    tag = L"[ERROR] ";
                    break;
                }

                std::wstring message = MsgStream.str();
                line = std::format(L"{}{}{}", timestamp, tag, message);

                if (bOutputToStdOut || bOutputToDebugConsole)
                {
                    std::wstring wModuleName(ModuleName.begin(), ModuleName.end());
                    std::wstring outstring = std::format(L"[{}] {}{}\n", wModuleName, tag, message);
                    if (bOutputToStdOut)
                    {
                        WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), outstring.c_str(), outstring.size(), NULL, NULL);
                    }
                    if (bOutputToDebugConsole)
                    {
                        OutputDebugStringW(outstring.c_str());
                    }
                }
            }
            else
            {
                line = MsgStream.str();
                if (bOutputToStdOut)
                {
                    WriteConsoleW(GetStdHandle(STD_OUTPUT_HANDLE), line.c_str(), line.size(), NULL, NULL);
                }
                if (bOutputToDebugConsole)
                {
                    OutputDebugStringW(line.c_str());
                }
            }

            std::string logString = ConvertUTF8(line);

            if (bWriteToFile)
            {
                ULog::Get().ClearFile();
                std::lock_guard lock(ULog::Get().file_mtx);
                std::fstream file(FileName, std::ios_base::app | std::ios_base::binary);
                file << logString << "\n";
                file.close();
            }

            if (Callback)
            {
                Callback(logString.c_str(), logString.size());
            }
        }

        template <typename T>
        inline UMessage& operator<<(T);
    };
};

template<typename T>
inline ULog::UMessage& ULog::UMessage::operator<<(T value)
{
    if (!(Type == EItemType::LOG_TYPE_DEBUG && !IS_DEBUG))
    {
        MsgStream << value;
    }
    return *this;
}

template<>
inline ULog::UMessage& ULog::UMessage::operator<<(std::string string)
{
    return *this << string.c_str();
}

inline std::filesystem::path ULog::FileName = "unknown_module.log";
inline std::string ULog::ModuleName = "UnknownModule";
inline bool ULog::bShowTime = true;
inline bool ULog::bWriteToFile = false;
inline bool ULog::bOutputToStdOut = true;
inline bool ULog::bOutputToDebugConsole = false;


template <size_t bufferSize = 1000, typename TChar, typename TSize, typename TParam>
inline std::basic_string<TChar> GetWinAPIString(TSize(*fp)(TParam, TChar*, TSize), TParam arg) // __stdcall aka. WINAPI is ignored on x64
{
    TChar buffer[bufferSize];
    TSize outSize = fp(arg, buffer, bufferSize);
    return std::basic_string<TChar>(buffer, outSize);
}

template <size_t bufferSize = 1000, typename TChar, typename TSize>
inline std::basic_string<TChar> GetWinAPIString(TSize(*fp)(TSize, TChar*))
{
    TChar buffer[bufferSize];
    TSize outSize = fp(bufferSize, buffer);
    return std::basic_string<TChar>(buffer, outSize);
}

template <size_t bufferSize = 1000, typename TChar, typename TSize>
inline std::basic_string<TChar> GetWinAPIString(TSize(*fp)(TChar*, TSize))
{
    TChar buffer[bufferSize];
    TSize outSize = fp(buffer, bufferSize);
    return std::basic_string<TChar>(buffer, outSize);
}

// Returns true once a matching file is modified, false when ReadDirectoryChanges fails
// pattern string must be in lower case
inline bool WaitForChanges(const std::filesystem::path& directory, const std::wregex& pattern, DWORD filter = FILE_NOTIFY_CHANGE_LAST_WRITE | FILE_NOTIFY_CHANGE_FILE_NAME)
{
#ifdef _DEBUG
	{
		static std::unordered_set<std::filesystem::path> dirs;
		if (!dirs.count(directory))
		{
			dirs.insert(directory);
			ULog::UMessage(ULog::EItemType::LOG_TYPE_DEBUG) << "Watching directory " << directory;
		}
	}
#endif
	static constexpr size_t FILE_NOTIFY_BUFFER_SIZE = 1024;
	while (1)
	{
		char notifybuf[FILE_NOTIFY_BUFFER_SIZE];
		DWORD numBytes;
		HANDLE hDir = CreateFileW(directory.wstring().c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, 0);
		if (ReadDirectoryChangesW(hDir, &notifybuf, FILE_NOTIFY_BUFFER_SIZE, FALSE, filter, &numBytes, 0, 0))
		{
			FILE_NOTIFY_INFORMATION* pNotify = reinterpret_cast<FILE_NOTIFY_INFORMATION*>(&notifybuf);
			if (numBytes)
			{
				while (1)
				{
					if (pNotify->Action == FILE_ACTION_MODIFIED || pNotify->Action == FILE_ACTION_RENAMED_NEW_NAME || pNotify->Action == FILE_ACTION_ADDED)
					{
						std::wstring wsName(pNotify->FileName, pNotify->FileNameLength / sizeof(WCHAR));
						std::transform(wsName.begin(), wsName.end(), wsName.begin(), [](wchar_t c) { return towlower(c); });
						if (std::regex_match(wsName, pattern))
						{
							return true;
						}
					}
					if (pNotify->NextEntryOffset == 0)
						break;
					pNotify += pNotify->NextEntryOffset;
				}
			}
			else
			{
				ULog::UMessage(ULog::EItemType::LOG_TYPE_ERROR) << "Couldn't setup config file watch";
				return false;
			}
		}
        else
        {
            return false;
        }
	}
	return false;
}

inline std::string ConvertUTF8(std::wstring inWStr)
{
    if (!inWStr.size())
    {
        return "";
    }

    std::string outStr{};
    int outSize = WideCharToMultiByte(CP_UTF8, WC_COMPOSITECHECK, inWStr.c_str(), inWStr.size(), nullptr, 0, nullptr, nullptr);
    assert(outSize);
    if (!outSize)
    {
        return outStr;
    }

    outStr.resize(outSize);
    int numBytes = WideCharToMultiByte(CP_UTF8, WC_COMPOSITECHECK, inWStr.c_str(), inWStr.size(), outStr.data(), outSize, nullptr, nullptr);
    assert(numBytes);
    return outStr;
}

inline std::string GetFilenameFromPath(std::string path, bool bRemoveExtension = true)
{
    std::string filename = path;
    size_t pos;
    pos = filename.rfind('\\');
    if (pos != std::string::npos)
    {
        filename = filename.substr(pos + 1);
    }
    if (bRemoveExtension)
    {
        pos = filename.rfind('.');
        if (pos != std::string::npos)
        {
            filename = filename.substr(0, pos);
        }
    }
    return filename;
}

inline HMODULE GetBaseModule(DWORD processId = 0)
{
    static HMODULE hModule = 0;

    // For subsequent calls just return the first value we got
    if (hModule)
    {
        return hModule;
    }

    if (processId == 0)
    {
        processId = GetCurrentProcessId();
    }

    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processId);
    if (!hProcess)
    {
        return 0;
    }

    DWORD dwSizeModules;
    // get The number of bytes required to store all module handles in the lphModule array
    EnumProcessModules(hProcess, nullptr, 0, &dwSizeModules);
    if (!dwSizeModules)
    {
		CloseHandle(hProcess);
		return 0;
    }

    HMODULE* Modules = (HMODULE*)malloc(dwSizeModules);
    if (Modules)
    {
        if (EnumProcessModules(hProcess, Modules, dwSizeModules, &dwSizeModules))
        {
            hModule = Modules[0];
        }
        free(Modules);
    }

	CloseHandle(hProcess);
	return hModule;
}

inline MODULEINFO GetBaseModuleInfo()
{
    static MODULEINFO mod;
    static bool bHasInfo = false;
    if (bHasInfo)
    {
        return mod;
    }
	HANDLE h = OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessId());
	GetModuleInformation(h, GetBaseModule(), &mod, sizeof(mod));
	CloseHandle(h);
	bHasInfo = true;
    return mod;
}

// checks if address is in base module
inline bool IsBaseAddress(LPVOID p)
{
    MODULEINFO mod = GetBaseModuleInfo();
    return (UINT_PTR(p) >= UINT_PTR(mod.lpBaseOfDll))
        && (UINT_PTR(p) <= (UINT_PTR(mod.lpBaseOfDll) + UINT_PTR(mod.SizeOfImage)));
}

// checks if address is in current dll
inline bool IsCurrentModuleAddress(LPVOID p, HMODULE hModule)
{
    MODULEINFO mod;
	HANDLE h = OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessId());
	GetModuleInformation(h, hModule, &mod, sizeof(mod));
	CloseHandle(h);
	UINT_PTR addr = reinterpret_cast<UINT_PTR>(p);
    return (addr >= UINT_PTR(mod.lpBaseOfDll)) && (addr <= (UINT_PTR(mod.lpBaseOfDll) + UINT_PTR(mod.SizeOfImage)));
}

inline PVOID GetRelativeAddress(PVOID absAddr)
{
    return PVOID(UINT_PTR(absAddr) - UINT_PTR(GetBaseModule()));
}

inline std::vector<uint16_t> StringtoScanPattern(std::string patternString)
{
    patternString.erase(std::remove_if(patternString.begin(), patternString.end(), [](unsigned char c) { return std::isspace(c); }), patternString.end());
    //if (patternString.size() % 2 != 0) patternString.pop_back();
    std::vector<uint16_t> out(patternString.size() / 2);
    for (size_t i = 0; i < out.size(); ++i)
    {
        std::string sbyte = std::string(&*(patternString.begin() + i * 2), 2);
        if (sbyte.find('?') != std::string::npos)
        {
            out[i] = 0xFF00;
        }
        else
        {
            out[i] = uint16_t(0x00FF) & (uint16_t)std::stoul(sbyte, nullptr, 16);
        }
    }
    return out;
}

inline std::vector<LPVOID> MemPatternScan(LPVOID lpOptBase, std::vector<uint16_t> pattern, bool bScanAllModules = false, size_t MaxMatches = 0, bool writeLogs = true)
{
    if (!lpOptBase)
    {
        lpOptBase = GetBaseModule();
    }

    std::vector<LPVOID> OutMatches;
    LPBYTE lpRegionBase = (LPBYTE)lpOptBase;

    ULog& Log = ULog::Get();

    std::stringstream ssPattern;
    for (uint16_t byte : pattern)
    {
        if (byte > uint16_t(0xff))
        {
            ssPattern << "??";
        }
        else
        {
            ssPattern << std::setfill('0') << std::setw(2) << std::hex << byte;
        }
        ssPattern << " ";
    }
    if (writeLogs)
        Log.println("Search pattern: %s", ssPattern.str().c_str());

    LPBYTE currentAddress = 0;
    while (true)
    {
        MEMORY_BASIC_INFORMATION memoryInfo = { 0 };
        if (VirtualQuery((void*)lpRegionBase, &memoryInfo, sizeof(MEMORY_BASIC_INFORMATION)) == 0)
        {
            if (OutMatches.size() > 0)
            {
                // Just stop quietly when something's already been found.
                break;
            }
            DWORD error = GetLastError();
            if (error == ERROR_INVALID_PARAMETER)
            {
                if (writeLogs)
                    Log.println("End of process memory.");
            }
            else
            {
                if (writeLogs)
                    Log.println("VirtualQuery error: %i.", error);
            }
            break;
        }
        lpRegionBase = (LPBYTE)memoryInfo.BaseAddress;

        // https://learn.microsoft.com/en-us/windows/win32/memory/memory-protection-constants
        bool bIsValidMem = memoryInfo.State == MEM_COMMIT &&
            (memoryInfo.Protect & 0xFF) != 0;

        bool bShouldScan = bScanAllModules || memoryInfo.AllocationBase == lpOptBase;

        //CHAR moduleName[100];
        //GetModuleFileNameA((HMODULE)memoryInfo.AllocationBase, moduleName, 100);
        std::string moduleName = GetFilenameFromPath(GetWinAPIString(GetModuleFileNameA, (HMODULE)memoryInfo.AllocationBase), false);

        if (bIsValidMem && bShouldScan)
        {
            if (writeLogs)
                Log.println("Searching region: %p %d %s", lpRegionBase, memoryInfo.RegionSize, moduleName.c_str());
            currentAddress = lpRegionBase;
            while (currentAddress < (lpRegionBase + memoryInfo.RegionSize) - pattern.size())
            {
                for (size_t i = 0; i < pattern.size(); i++)
                {
                    uint8_t bitmask = ~uint8_t(pattern[i] >> 8);
                    bool bByteMatches = ((*(uint8_t*)currentAddress) & bitmask) == (uint8_t(pattern[i] & 0xff) & bitmask);
                    ++currentAddress;
                    if (!bByteMatches)
                    {
                        break;
                    }
                    if (i == pattern.size() - 1)
                    {
                        LPVOID lpMatch = currentAddress - pattern.size();
                        if (writeLogs)
                            Log.println("Found signature at %p", lpMatch);
                        OutMatches.push_back(lpMatch);
                        break;
                    }
                }
            }
        }
        else
        {
            if (writeLogs)
                Log.dprintln("Skipping region: %p %d %d %s", lpRegionBase, bIsValidMem, bShouldScan, moduleName.c_str());
        }

        if (MaxMatches > 0 && OutMatches.size() >= MaxMatches)
        {
            break;
        }

        lpRegionBase += memoryInfo.RegionSize;
    }
    OutMatches.shrink_to_fit();

    return OutMatches;
}

// 5 bytes call/jmp
inline LPVOID GetJumpTargetNear(LPVOID pInstruction)
{
    if (pInstruction == nullptr)
    {
        return nullptr;
    }
    UINT_PTR address = reinterpret_cast<UINT_PTR>(pInstruction);
    BYTE op = *reinterpret_cast<BYTE*>(address);
    if (op == 0xE8/*call*/ || op == 0xE9/*jmp*/)
    {
        INT32 offset = *reinterpret_cast<INT32*>(address + 1);
        return reinterpret_cast<LPVOID>(address + 5 + offset);
    }
    return nullptr;
}

inline LPVOID GetJumpTargetFar(LPVOID pInstruction)
{
    if (pInstruction == nullptr)
    {
        return nullptr;
    }
    UINT_PTR address = reinterpret_cast<UINT_PTR>(pInstruction);
    UINT16 op = *reinterpret_cast<UINT16*>(address);
    if (op == 0x25ff)
    {
        return *reinterpret_cast<LPVOID*>(address + 6);
    }
    return nullptr;
}

/// @brief Find out which static address a instruction accesses
/// @param instructionStart Instruction pointer
/// @param instructionSize Length of the instruction
/// @param relAddressOffset Position of the displacement
/// @return
inline void* GetRelativeImmediateAddress(void* instructionStart, unsigned int instructionSize, unsigned int relAddressOffset)
{
	int32_t relAddress = *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(instructionStart) + relAddressOffset);
	return reinterpret_cast<void*>(reinterpret_cast<uint64_t>(instructionStart) + instructionSize + relAddress);
}

// Find the target address of a call instruction
inline LPVOID FindCallTarget(LPVOID pOptBase, std::vector<uint16_t> pattern, int offset = 0, bool bScanAllModules = false)
{
    std::vector<LPVOID> pScan = MemPatternScan(pOptBase, pattern, bScanAllModules, 1);
    if (pScan.size() && pScan[0])
    {
        static constexpr SIZE_T CALL_SIZE = 5;
        static constexpr SIZE_T CALL_OP = 1;
        {
            UINT_PTR Call = UINT_PTR(pScan[0]) + offset;
            ULog::Get().println("Decoding call: %p", Call);
            INT32 RelAddr = *reinterpret_cast<INT32*>(Call + CALL_OP); // Must be signed
            LPVOID pTarget = reinterpret_cast<LPVOID>(Call + CALL_SIZE + RelAddr);
            ULog::Get().println("Call target: %p", pTarget);
            return pTarget;
        }
    }
    return nullptr;
}

// Find the target address of a call instruction
inline LPVOID FindCallTarget(LPVOID lpOptBase, std::string pattern, int offset = 0, bool bScanAllModules = false)
{
    return FindCallTarget(lpOptBase, StringtoScanPattern(pattern), offset, bScanAllModules);
}

inline std::string GetDLLName(HMODULE hModule = nullptr)
{
    return GetFilenameFromPath(GetWinAPIString(GetModuleFileNameA, hModule));
}

// @return HMODULE Handle to the module that this function is executed in
inline HMODULE GetCurrentModule()
{
    HMODULE hModule = NULL;
    // seems hack-ish
    GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, reinterpret_cast<LPCSTR>(&GetCurrentModule), &hModule);
    return hModule;
}

inline HMODULE GetModuleFromPtr(void* p = nullptr)
{
    HMODULE hModule = NULL;
    GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS, p ? reinterpret_cast<LPCSTR>(p) : reinterpret_cast<LPCSTR>(&GetModuleFromPtr), &hModule);
    return hModule;
}

inline HMODULE LoadLibraryString(std::string libFileName)
{
    return LoadLibraryA(libFileName.c_str());
}

inline HMODULE LoadLibraryString(std::wstring libFileName)
{
    return LoadLibraryW(libFileName.c_str());
}

// Tries to loads a DLL and shows an error popup when failed.
// @param std::filesystem::path path: relative or absolute path to library
// @param bool relativeToModule: whether the library path is relative to current module instead of the current process
// @param HWND hwnd: window handle passed to MessageBox
// @return HMODULE The return value of LoadLibraryA|LoadLibraryW
inline HMODULE TryLoadLibrary(std::filesystem::path path, bool relativeToModule = true, HWND hwnd = NULL)
{
    if (path.is_relative())
    {
        std::filesystem::path pathThis(GetWinAPIString(GetModuleFileNameW, relativeToModule ? GetCurrentModule() : NULL));
        path = pathThis.parent_path() / path;
    }
    HMODULE hModule = LoadLibraryW(path.wstring().c_str());
    if (!hModule)
    {
        DWORD error = GetLastError();
        std::wstring caption = std::filesystem::path(GetWinAPIString(GetModuleFileNameW, GetCurrentModule())).filename();
        MessageBoxW(hwnd, std::format(L"Failed to load \"{}\"", path.filename().wstring().c_str()).c_str(), caption.c_str(), (hwnd ? MB_APPLMODAL : MB_SYSTEMMODAL) | MB_ICONERROR);
        SetLastError(error);
    }
    return hModule;
}

// does not contain a trailing backslash
inline std::string GetDLLDirectory(HMODULE hModule = nullptr)
{
    static std::string path = "";
    if (!path.empty())
    {
        return path;
    }

    path = GetWinAPIString(GetModuleFileNameA, hModule);
    size_t pos = path.rfind('\\');
    if (pos != std::string::npos)
    {
        path = path.substr(0, pos);
    }
    return path;
}

// if findSymlinkIfNotInCWD is true and the DLL path is not in the working directory
// search in CWD for the symlink pointing to the DLL file
inline std::filesystem::path GetDLLPath(HMODULE hModule, bool findSymlinkIfNotInCWD = false)
{
    static std::filesystem::path outPath = {};
    if (!outPath.empty())
    {
        return outPath;
    }

    std::filesystem::path modulePath = GetWinAPIString(GetModuleFileNameW, hModule);
    outPath = modulePath;

    std::filesystem::path cwd = std::filesystem::current_path();
    std::filesystem::path moduleDir = modulePath.parent_path();
    auto mismatch = std::mismatch(moduleDir.begin(), moduleDir.end(), cwd.begin(), cwd.end());
    bool isModuleInCWD = mismatch.second == cwd.end();

    if (!isModuleInCWD && findSymlinkIfNotInCWD)
    {
        for (auto const& dir_entry : std::filesystem::recursive_directory_iterator(cwd))
        {
            if (dir_entry.is_symlink())
            {
                if (std::filesystem::read_symlink(dir_entry) == modulePath)
                {
                    outPath = dir_entry;
                    break;
                }
            }
        }
    }
    return outPath;
}

struct UParamEnumWnd
{
    HWND LastHWnd;
    std::wstring Title;
    bool bUseClassName = false;
};
inline bool CheckWndText(HWND hwnd, UParamEnumWnd *enumInfo)
{
    if (enumInfo->bUseClassName)
    {
        std::wstring windowClass = GetWinAPIString(GetClassNameW, hwnd);
        ULog::UMessage(ULog::EItemType::LOG_TYPE_DEBUG) << "window class: " << windowClass << " " << reinterpret_cast<LPVOID>(hwnd);
        if (windowClass.find(enumInfo->Title) != std::wstring::npos)
        {
            enumInfo->LastHWnd = hwnd;
            return true;
        }
    }
    else
    {
        // check if the target window can process messages and wait till it can
        // however, this doesn't seem to work and the process hangs anyway
        for (; /*IsHungAppWindow(hwnd) || */!SendMessageTimeoutW(hwnd, WM_NULL, NULL, NULL, SMTO_NORMAL, 1000, NULL);)
        {
            ULog::Get().dprintln("waiting for window to become responsive");
            Sleep(4000);
        }
        std::wstring windowTitle = GetWinAPIString(GetWindowTextW, hwnd);
        ULog::UMessage(ULog::EItemType::LOG_TYPE_DEBUG) << "window title: " << windowTitle << " " << reinterpret_cast<LPVOID>(hwnd);
        if (windowTitle.find(enumInfo->Title) != std::wstring::npos)
        {
            enumInfo->LastHWnd = hwnd;
            return true;
        }
    }
    return false;
}

inline BOOL CALLBACK EnumWndCallback(HWND hwnd, LPARAM param)
{
    DWORD procID = 0;
    GetWindowThreadProcessId(hwnd, &procID);
    if (procID == GetCurrentProcessId())
    {
        UParamEnumWnd* pInfo = (UParamEnumWnd*)param;
        if (pInfo->Title.size() == 0)
        {
            pInfo->LastHWnd = hwnd;
            return FALSE;
        }
        if (CheckWndText(hwnd, pInfo))
        {
            return FALSE;
        }
    }
    return TRUE;
}

// USE WITH CAUTION. This can cause any window in the current process to hang indefinitely if it's already unresponsive.
// @return HWND Handle to the first window with a matching title
// @param wstring title: can be an empty string, for which the function will return the first window belonging to the current process
// @param bool useClassName: search for the window using class names instead of window titles
inline HWND FindWindowHandle(std::wstring title = L"", bool useClassName = false)
{
    static std::unordered_map<std::wstring, HWND> Results;
    if (title.size() && Results.contains(title))
    {
        return Results[title];
    }
    UParamEnumWnd info(nullptr, title, useClassName);
    EnumWindows(&EnumWndCallback, LPARAM(&info));
    Results[title] = info.LastHWnd;
    return info.LastHWnd;
}

#define LOG_FIRST_CALL(fp, paramsFmt, ...)\
{\
    static bool bCalled_ ## fp = false;\
    if (!bCalled_ ## fp)\
    {\
        bCalled_ ## fp = true;\
        ULog::Get().println("First hook call: %p %s, arguments: " paramsFmt, fp, #fp, __VA_ARGS__);\
    }\
}


inline std::vector<uint8_t> UTF16ToAOB(std::u16string s16)
{
    std::vector<uint8_t> out;
    for (char16_t c : s16)
    {
        out.push_back(c & 0xff);
        out.push_back(c >> 8);
    }
    return out;
}

MODUTILS_NAMESPACE_END
#undef MODUTILS_NAMESPACE_BEGIN
#undef MODUTILS_NAMESPACE_END
