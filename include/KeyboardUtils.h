#pragma once

#include <algorithm>
#include <format>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <cstring>

#include "ModUtils.h"

#if !KEYBOARDUTILS_GLOBAL_NAMESPACE
#ifndef KEYBOARDUTILS_NAMESPACE
#define KEYBOARDUTILS_NAMESPACE KeyboardUtils
#endif
#define KEYBOARDUTILS_NAMESPACE_BEGIN namespace KEYBOARDUTILS_NAMESPACE {
#define KEYBOARDUTILS_NAMESPACE_END }
#else
#define KEYBOARDUTILS_NAMESPACE
#define KEYBOARDUTILS_NAMESPACE_BEGIN
#define KEYBOARDUTILS_NAMESPACE_END
#endif

KEYBOARDUTILS_NAMESPACE_BEGIN

// there must be a better way...
inline const static std::map<std::string, int> KEY_MAP{
	//{"lbutton"			, VK_LBUTTON	}, //Left mouse button
	//{"mouse left"		, VK_LBUTTON	}, //Left mouse button
	//{"mouseleft"		, VK_LBUTTON	}, //Left mouse button
	//{"rbutton"			, VK_RBUTTON	}, //Right mouse button
	//{"mouse right"		, VK_RBUTTON	}, //Right mouse button
	//{"mouseright"		, VK_RBUTTON	}, //Right mouse button
	//{"cancel"			, VK_CANCEL		}, //Control - break processing
	//{"mbutton"			, VK_MBUTTON	}, //Middle mouse button
	//{"mouse middle"		, VK_MBUTTON	}, //Middle mouse button
	//{"mousemiddle"		, VK_MBUTTON	}, //Middle mouse button
	//{"xbutton1"			, VK_XBUTTON1	}, //X1 mouse button
	//{"xbutton2"			, VK_XBUTTON2	}, //X2 mouse button
	{"back",           0x08}, // BACKSPACE key
	{"backspace",      0x08}, // BACKSPACE key
	{"bckspc",         0x08}, // BACKSPACE key
	{"bcksp",          0x08}, // BACKSPACE key
	{"bksp",           0x08}, // BACKSPACE key
	{"tab",            0x09}, // TAB key
	{"clear",          0x0C}, // CLEAR key
	{"return",         0x0D}, // ENTER key
	{"enter",          0x0D}, // ENTER key
	{"shift",          0x10}, // SHIFT key
	{"control",        0x11}, // CTRL key
	{"ctrl",           0x11}, // CTRL key
	{"menu",           0x12}, // ALT key
	{"alt",            0x12}, // ALT key
	{"pause",          0x13}, // PAUSE key
	{"capital",        0x14}, // CAPS LOCK key
	{"caps lock",      0x14}, // CAPS LOCK key
	{"capslock",       0x14}, // CAPS LOCK key
	{"capslck",        0x14}, // CAPS LOCK key
	{"capslk",         0x14}, // CAPS LOCK key
	{"caps",           0x14}, // CAPS LOCK key
	{"escape",         0x1B}, // ESC key
	{"esc",            0x1B}, // ESC key
	{"space",          0x20}, // SPACEBAR
	{"spacebar",       0x20}, // SPACEBAR
	{"prior",          0x21}, // PAGE UP key
	{"page up",        0x21}, // PAGE UP key
	{"pageup",         0x21}, // PAGE UP key
	{"pgup",           0x21}, // PAGE UP key
	{"next",           0x22}, // PAGE DOWN key
	{"page down",      0x22}, // PAGE DOWN key
	{"pagedown",       0x22}, // PAGE DOWN key
	{"pgdown",         0x22}, // PAGE DOWN key
	{"pgdn",           0x22}, // PAGE DOWN key
	{"end",            0x23}, // END key
	{"home",           0x24}, // HOME key
	{"left",           0x25}, // LEFT ARROW key
	{"up",             0x26}, // UP ARROW key
	{"right",          0x27}, // RIGHT ARROW key
	{"down",           0x28}, // DOWN ARROW key
	{"select",         0x29}, // SELECT key
	{"print",          0x2A}, // PRINT key
	{"execute",        0x2B}, // EXECUTE key
	{"snapshot",       0x2C}, // PRINT SCREEN key
	{"print screen",   0x2C}, // PRINT SCREEN key
	{"printscreen",    0x2C}, // PRINT SCREEN key
	{"prtscrn",        0x2C}, // PRINT SCREEN key
	{"prtscr",         0x2C}, // PRINT SCREEN key
	{"prtscn",         0x2C}, // PRINT SCREEN key
	{"prtsc",          0x2C}, // PRINT SCREEN key
	{"prscr",          0x2C}, // PRINT SCREEN key
	{"prsc",           0x2C}, // PRINT SCREEN key
	{"ps",             0x2C}, // PRINT SCREEN key
	{"insert",         0x2D}, // INS key
	{"ins",            0x2D}, // INS key
	{"delete",         0x2E}, // DEL key
	{"del",            0x2E}, // DEL key
	{"help",           0x2F}, // HELP key
	{"0",			  0x30},
	// (rest of the numbers row)
	{"a",			  0x41},
	// (rest of the letters)
	{"lwin",           0x5B}, // Left Windows key
	{"lsuper",         0x5B}, // Left Windows key
	{"lmeta",          0x5B}, // Left Windows key
	{"left windows",   0x5B}, // Left Windows key
	{"left win",       0x5B}, // Left Windows key
	{"left super",     0x5B}, // Left Windows key
	{"left meta",      0x5B}, // Left Windows key
	{"leftwindows",    0x5B}, // Left Windows key
	{"leftwin",        0x5B}, // Left Windows key
	{"leftsuper",      0x5B}, // Left Windows key
	{"leftmeta",       0x5B}, // Left Windows key
	{"rwin",           0x5C}, // Right Windows key
	{"rsuper",         0x5C}, // Right Windows key
	{"rmeta",          0x5C}, // Right Windows key
	{"right windows",  0x5C}, // Right Windows key
	{"right win",      0x5C}, // Right Windows key
	{"right super",    0x5C}, // Right Windows key
	{"right meta",     0x5C}, // Right Windows key
	{"rightwindows",   0x5C}, // Right Windows key
	{"rightwin",       0x5C}, // Right Windows key
	{"rightsuper",     0x5C}, // Right Windows key
	{"rightmeta",      0x5C}, // Right Windows key
	{"apps",           0x5D}, // Applications key
	{"sleep",          0x5F}, // Computer Sleep key
	{"numpad0",        0x60}, // Numeric keypad 0 key
	{"numpad1",        0x61}, // Numeric keypad 1 key
	{"numpad2",        0x62}, // Numeric keypad 2 key
	{"numpad3",        0x63}, // Numeric keypad 3 key
	{"numpad4",        0x64}, // Numeric keypad 4 key
	{"numpad5",        0x65}, // Numeric keypad 5 key
	{"numpad6",        0x66}, // Numeric keypad 6 key
	{"numpad7",        0x67}, // Numeric keypad 7 key
	{"numpad8",        0x68}, // Numeric keypad 8 key
	{"numpad9",        0x69}, // Numeric keypad 9 key
	{"num0",           0x60}, // Numeric keypad 0 key
	{"num1",           0x61}, // Numeric keypad 1 key
	{"num2",           0x62}, // Numeric keypad 2 key
	{"num3",           0x63}, // Numeric keypad 3 key
	{"num4",           0x64}, // Numeric keypad 4 key
	{"num5",           0x65}, // Numeric keypad 5 key
	{"num6",           0x66}, // Numeric keypad 6 key
	{"num7",           0x67}, // Numeric keypad 7 key
	{"num8",           0x68}, // Numeric keypad 8 key
	{"num9",           0x69}, // Numeric keypad 9 key
	{"np0",            0x60}, // Numeric keypad 0 key
	{"np1",            0x61}, // Numeric keypad 1 key
	{"np2",            0x62}, // Numeric keypad 2 key
	{"np3",            0x63}, // Numeric keypad 3 key
	{"np4",            0x64}, // Numeric keypad 4 key
	{"np5",            0x65}, // Numeric keypad 5 key
	{"np6",            0x66}, // Numeric keypad 6 key
	{"np7",            0x67}, // Numeric keypad 7 key
	{"np8",            0x68}, // Numeric keypad 8 key
	{"np9",            0x69}, // Numeric keypad 9 key
	{"multiply",       0x6A}, // Multiply key
	{"add",            0x6B}, // Add key
	{"separator",      0x6C}, // Separator key
	{"subtract",       0x6D}, // Subtract key
	{"subtract",       0x6D}, // Subtract key
	{"decimal",        0x6E}, // Decimal key
	{"divide",         0x6F}, // Divide key
	{"f1",             0x70}, // F1 key
	{"f2",             0x71}, // F2 key
	{"f3",             0x72}, // F3 key
	{"f4",             0x73}, // F4 key
	{"f5",             0x74}, // F5 key
	{"f6",             0x75}, // F6 key
	{"f7",             0x76}, // F7 key
	{"f8",             0x77}, // F8 key
	{"f9",             0x78}, // F9 key
	{"f10",            0x79}, // F10 key
	{"f11",            0x7A}, // F11 key
	{"f12",            0x7B}, // F12 key
	{"f13",            0x7C}, // F13 key
	{"f14",            0x7D}, // F14 key
	{"f15",            0x7E}, // F15 key
	{"f16",            0x7F}, // F16 key
	{"f17",            0x80}, // F17 key
	{"f18",            0x81}, // F18 key
	{"f19",            0x82}, // F19 key
	{"f20",            0x83}, // F20 key
	{"f21",            0x84}, // F21 key
	{"f22",            0x85}, // F22 key
	{"f23",            0x86}, // F23 key
	{"f24",            0x87}, // F24 key
	{"numlk",          0x90}, // NUM LOCK key
	{"numlock",        0x90}, // NUM LOCK key
	{"num lock",       0x90}, // NUM LOCK key
	{"scroll",         0x91}, // SCROLL LOCK key
	{"scrolllock",     0x91}, // SCROLL LOCK key
	{"scroll lock",    0x91}, // SCROLL LOCK key
	// not available in WNDPROC
	{"lshift",         0xA0}, // Left SHIFT key
	{"left shift",     0xA0}, // Left SHIFT key
	{"leftshift",      0xA0}, // Left SHIFT key
	{"rshift",         0xA1}, // Right SHIFT key
	{"right shift",    0xA1}, // Right SHIFT key
	{"rightshift",     0xA1}, // Right SHIFT key
	{"lcontrol",       0xA2}, // Left CONTROL key
	{"left control",   0xA2}, // Left CONTROL key
	{"leftcontrol",    0xA2}, // Left CONTROL key
	{"rcontrol",       0xA3}, // Right CONTROL key
	{"right control",  0xA3}, // Right CONTROL key
	{"rightcontrol",   0xA3}, // Right CONTROL key
	{"lmenu",          0xA4}, // Left ALT key
	{"lalt",           0xA4}, // Left ALT key
	{"left alt",       0xA4}, // Left ALT key
	{"leftalt",        0xA4}, // Left ALT key
	{"rmenu",          0xA5}, // Right ALT key
	{"ralt",           0xA5}, // Right ALT key
	{"right alt",      0xA5}, // Right ALT key
	{"rightalt",       0xA5}, // Right ALT key
	// END not available in WNDPROC
	{"semicolon",      0xBA}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the; : key
	{";",			  0xBA}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the; : key
	{"equals sign",    0xBB}, // For any country / region, the + key
	{"equalssign",     0xBB}, // For any country / region, the + key
	{"equal sign",     0xBB}, // For any country / region, the + key
	{"equalsign",      0xBB}, // For any country / region, the + key
	{"equals",         0xBB}, // For any country / region, the + key
	{"equal",          0xBB}, // For any country / region, the + key
	{"=",			  0xBB}, // For any country / region, the + key
	{"comma",          0xBC}, // For any country / region, the, key
	{",",			  0xBC}, // For any country / region, the, key
	{"minus",          0xBD}, // For any country / region, the - key
	{"hyphen",         0xBD}, // For any country / region, the - key
	{"dash",           0xBD}, // For any country / region, the - key
	{"-",			  0xBD}, // For any country / region, the - key
	{"period",         0xBE}, // For any country / region, the.key
	{"dot",            0xBE}, // For any country / region, the.key
	{".",			  0xBE}, // For any country / region, the.key
	{"forward slash",  0xBF}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the / ? key
	{"forwardslash",   0xBF}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the / ? key
	{"slash",		   0xBF}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the / ? key
	{"/",			  0xBF}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the / ? key
	{"backtick",       0xC0}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the `~ key
	{"tick",           0xC0}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the `~ key
	{"`",			  0xC0}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the `~ key
	{"left bracket",   0xDB}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the[{ key
	{"leftbracket",    0xDB}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the[{ key
	{"[",			  0xDB}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the[{ key
	{"backward slash", 0xDC}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the \\ | key
	{"backwardslash",  0xDC}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the \\ | key
	{"\\",             0xDC}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the \\ | key
	{"right bracket",  0xDD}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the]} key
	{"rightbracket",   0xDD}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the]} key
	{"]",			  0xDD}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the]} key
	{"quote",          0xDE}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the '" key
	{"'",			  0xDE}, // Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the '" key
};

inline const static std::map<int, std::string> KEY_LABELS = {
	{ VK_LBUTTON 				, "Mouse Left"},			//Left mouse button
	{ VK_RBUTTON 				, "Mouse Right"},			//Right mouse button
	{ VK_CANCEL 				, "Cancel"},				//Control - break processing
	{ VK_MBUTTON 				, "Mouse Middle"},			//Middle mouse button
	{ VK_XBUTTON1 				, "Mouse X1"},				//X1 mouse button
	{ VK_XBUTTON2 				, "Mouse X2"},				//X2 mouse button
	{ VK_BACK 					, "Backspace"},				//BACKSPACE key
	{ VK_TAB 					, "Tab"},					//TAB key
	{ VK_CLEAR 					, "Clear"},					//CLEAR key
	{ VK_RETURN 				, "Enter"},					//ENTER key
	{ VK_SHIFT 					, "Shift"},					//SHIFT key
	{ VK_CONTROL 				, "Ctrl"},					//CTRL key
	{ VK_MENU 					, "Alt"},					//ALT key
	{ VK_PAUSE 					, "Pause"},					//PAUSE key
	{ VK_CAPITAL 				, "Caps Lock"},				//CAPS LOCK key
	{ VK_KANA 					, "Kana"},					//IME Kana mode
	{ VK_HANGUL 				, "Hangul"},				//IME Hangul mode
	{ VK_IME_ON 				, "IME On"},				//IME On
	{ VK_JUNJA 					, "Junja"},					//IME Junja mode
	{ VK_FINAL 					, "Final"},					//IME final mode
	{ VK_HANJA 					, "Hanja"},					//IME Hanja mode
	{ VK_KANJI 					, "Kanji"},					//IME Kanji mode
	{ VK_IME_OFF 				, "IME Off"},				//IME Off
	{ VK_ESCAPE 				, "Esc"},					//ESC key
	{ VK_CONVERT 				, "Convert"},				//IME convert
	{ VK_NONCONVERT 			, "Nonconvert"},			//IME nonconvert
	{ VK_ACCEPT 				, "Accept"},				//IME accept
	{ VK_MODECHANGE 			, "Mode Change"},			//IME mode change request
	{ VK_SPACE 					, "Space"},					//SPACEBAR
	{ VK_PRIOR 					, "Page Up"},				//PAGE UP key
	{ VK_NEXT 					, "Page Down"},				//PAGE DOWN key
	{ VK_END 					, "End"},					//END key
	{ VK_HOME 					, "Home"},					//HOME key
	{ VK_LEFT 					, "Left"},					//LEFT ARROW key
	{ VK_UP 					, "Up"},					//UP ARROW key
	{ VK_RIGHT 					, "Right"},					//RIGHT ARROW key
	{ VK_DOWN 					, "Down"},					//DOWN ARROW key
	{ VK_SELECT 				, "Select"},				//SELECT key
	{ VK_PRINT 					, "Print"},					//PRINT key
	{ VK_EXECUTE 				, "Execute"},				//EXECUTE key
	{ VK_SNAPSHOT 				, "Print Screen"},			//PRINT SCREEN key
	{ VK_INSERT 				, "Insert"},				//INS key
	{ VK_DELETE 				, "Delete"},				//DEL key
	{ VK_HELP 					, "Help"},					//HELP key
	{ VK_LWIN 					, "Left Windows"},			//Left Windows key
	{ VK_RWIN 					, "Right Windows"},			//Right Windows key
	{ VK_APPS 					, "Application"},			//Applications key
	{ VK_SLEEP 					, "Sleep"},					//Computer Sleep key
	{ VK_NUMPAD0 				, "Numpad 0"},				//Numeric keypad 0 key
	{ VK_NUMPAD1 				, "Numpad 1"},				//Numeric keypad 1 key
	{ VK_NUMPAD2 				, "Numpad 2"},				//Numeric keypad 2 key
	{ VK_NUMPAD3 				, "Numpad 3"},				//Numeric keypad 3 key
	{ VK_NUMPAD4 				, "Numpad 4"},				//Numeric keypad 4 key
	{ VK_NUMPAD5 				, "Numpad 5"},				//Numeric keypad 5 key
	{ VK_NUMPAD6 				, "Numpad 6"},				//Numeric keypad 6 key
	{ VK_NUMPAD7 				, "Numpad 7"},				//Numeric keypad 7 key
	{ VK_NUMPAD8 				, "Numpad 8"},				//Numeric keypad 8 key
	{ VK_NUMPAD9 				, "Numpad 9"},				//Numeric keypad 9 key
	{ VK_MULTIPLY 				, "Multiply"},				//Multiply key
	{ VK_ADD 					, "Add"},					//Add key
	{ VK_SEPARATOR 				, "Separator"},				//Separator key
	{ VK_SUBTRACT 				, "Subtract"},				//Subtract key
	{ VK_DECIMAL 				, "Decimal"},				//Decimal key
	{ VK_DIVIDE 				, "Divide"},				//Divide key
	{ VK_F1 					, "F1"},					//F1 key
	{ VK_F2 					, "F2"},					//F2 key
	{ VK_F3 					, "F3"},					//F3 key
	{ VK_F4 					, "F4"},					//F4 key
	{ VK_F5 					, "F5"},					//F5 key
	{ VK_F6 					, "F6"},					//F6 key
	{ VK_F7 					, "F7"},					//F7 key
	{ VK_F8 					, "F8"},					//F8 key
	{ VK_F9 					, "F9"},					//F9 key
	{ VK_F10 					, "F10"},					//F10 key
	{ VK_F11 					, "F11"},					//F11 key
	{ VK_F12 					, "F12"},					//F12 key
	{ VK_F13 					, "F13"},					//F13 key
	{ VK_F14 					, "F14"},					//F14 key
	{ VK_F15 					, "F15"},					//F15 key
	{ VK_F16 					, "F16"},					//F16 key
	{ VK_F17 					, "F17"},					//F17 key
	{ VK_F18 					, "F18"},					//F18 key
	{ VK_F19 					, "F19"},					//F19 key
	{ VK_F20 					, "F20"},					//F20 key
	{ VK_F21 					, "F21"},					//F21 key
	{ VK_F22 					, "F22"},					//F22 key
	{ VK_F23 					, "F23"},					//F23 key
	{ VK_F24 					, "F24"},					//F24 key
	{ VK_NUMLOCK 				, "Num Lock"},				//NUM LOCK key
	{ VK_SCROLL 				, "Scroll Lock"},			//SCROLL LOCK key
	{ VK_LSHIFT 				, "Left Shift"},			//Left SHIFT key
	{ VK_RSHIFT 				, "Right Shift"},			//Right SHIFT key
	{ VK_LCONTROL 				, "Left Ctrl"},			//Left CONTROL key
	{ VK_RCONTROL 				, "Right Ctrl"},			//Right CONTROL key
	{ VK_LMENU 					, "Left Alt"},				//Left ALT key
	{ VK_RMENU 					, "Right Alt"},				//Right ALT key
	{ VK_BROWSER_BACK 			, "Browser Back"},			//Browser Back key
	{ VK_BROWSER_FORWARD 		, "Browser Forward"},		//Browser Forward key
	{ VK_BROWSER_REFRESH 		, "Browser Refresh"},		//Browser Refresh key
	{ VK_BROWSER_STOP 			, "Browser Stop"},			//Browser Stop key
	{ VK_BROWSER_SEARCH 		, "Browser Search"},		//Browser Search key
	{ VK_BROWSER_FAVORITES 		, "Browser Favorites"},		//Browser Favorites key
	{ VK_BROWSER_HOME 			, "Browser Home"},			//Browser Start and Home key
	{ VK_VOLUME_MUTE 			, "Volume Mute"},			//Volume Mute key
	{ VK_VOLUME_DOWN 			, "Volume Down"},			//Volume Down key
	{ VK_VOLUME_UP 				, "Volume Up"},				//Volume Up key
	{ VK_MEDIA_NEXT_TRACK 		, "Media Next"},			//Next Track key
	{ VK_MEDIA_PREV_TRACK 		, "Media Previous"},		//Previous Track key
	{ VK_MEDIA_STOP 			, "Media Stop"},			//Stop Media key
	{ VK_MEDIA_PLAY_PAUSE 		, "Media Play/Pause"},		//Play / Pause Media key
	{ VK_LAUNCH_MAIL 			, "Launch Mail"},			//Start Mail key
	{ VK_LAUNCH_MEDIA_SELECT 	, "Launch Media"},			//Select Media key
	{ VK_LAUNCH_APP1 			, "Launch App1"},			//Start Application 1 key
	{ VK_LAUNCH_APP2 			, "Launch App2"},			//Start Application 2 key
	{ VK_OEM_1 					, ";"},						//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the; : key
	{ VK_OEM_PLUS 				, "="},						//For any country / region, the + key
	{ VK_OEM_COMMA 				, ","},						//For any country / region, the , key
	{ VK_OEM_MINUS 				, "-"},						//For any country / region, the - key
	{ VK_OEM_PERIOD 			, "."},						//For any country / region, the.key
	{ VK_OEM_2 					, "/"},						//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the / ? key
	{ VK_OEM_3 					, "`"},						//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the `~ key
	{ VK_OEM_4 					, "["},						//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the[{ key
	{ VK_OEM_5 					, "\\"},					//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the \\ | key
	{ VK_OEM_6 					, "]"},						//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the]} key
	{ VK_OEM_7 					, "'"},						//Used for miscellaneous characters; it can vary by keyboard.For the US standard keyboard, the '" key
	{ VK_OEM_8 					, "Oem 8"},					//Used for miscellaneous characters; it can vary by keyboard.
	{ VK_OEM_102 				, "<>"},					//The <> keys on the US standard keyboard, or the \\ | key on the non - US 102 - key keyboard
	{ VK_PROCESSKEY 			, "Process"},				//IME PROCESS key
	{ VK_PACKET 				, "Packet"},				//Used to pass Unicode characters as if they were keystrokes.The VK_PACKET key is the low word of a 32 - bit Virtual Key value used for non - keyboard input methods.For more information, see Remark in KEYBDINPUT, SendInput, WM_KEYDOWN, and WM_KEYUP
	{ VK_ATTN 					, "Attn"},					//Attn key
	{ VK_CRSEL 					, "CrSel"},					//CrSel key
	{ VK_EXSEL 					, "ExSel"},					//ExSel key
	{ VK_EREOF 					, "Erase EOF"},				//Erase EOF key
	{ VK_PLAY 					, "Play"},					//Play key
	{ VK_ZOOM 					, "Zoom"},					//Zoom key
	{ VK_NONAME 				, "No name"},				//Reserved
	{ VK_PA1 					, "PA1"},					//PA1 key
	{ VK_OEM_CLEAR 				, "OEM Clear"},				//Clear key
};

inline const static std::map<std::string, int> MOUSE_BUTTONS{
	{"lbutton"				, MK_LBUTTON  },
	{"left mouse button"	, MK_LBUTTON  },
	{"left button"			, MK_LBUTTON  },
	{"button left"			, MK_LBUTTON  },
	{"mouse button left"	, MK_LBUTTON  },
	{"mouse left"			, MK_LBUTTON  },
	{"mouseleft"			, MK_LBUTTON  },
	{"lmb"					, MK_LBUTTON  },
	{"rbutton"				, MK_RBUTTON  },
	{"right mouse button"	, MK_RBUTTON  },
	{"right button"			, MK_RBUTTON  },
	{"button right"			, MK_RBUTTON  },
	{"mouse button right"	, MK_RBUTTON  },
	{"mouse right"			, MK_RBUTTON  },
	{"rmb"					, MK_RBUTTON  },
	{"mbutton"				, MK_MBUTTON  },
	{"middle mouse button"	, MK_MBUTTON  },
	{"middle button"		, MK_MBUTTON  },
	{"button middle"		, MK_MBUTTON  },
	{"mouse button middle"	, MK_MBUTTON  },
	{"mouse middle"			, MK_MBUTTON  },
	{"mmb"					, MK_MBUTTON  },
	{"xbutton1"				, MK_XBUTTON1 },
	{"x1 mouse button"		, MK_XBUTTON1 },
	{"x1 button"			, MK_XBUTTON1 },
	{"back mouse button"	, MK_XBUTTON1 },
	{"back button"			, MK_XBUTTON1 },
	{"button back"			, MK_XBUTTON1 },
	{"mouse button back"	, MK_XBUTTON1 },
	{"mouse back"			, MK_XBUTTON1 },
	{"xbutton2"				, MK_XBUTTON2 },
	{"x2 button"			, MK_XBUTTON2 },
	{"x2 mouse button"		, MK_XBUTTON2 },
	{"forward mouse button"	, MK_XBUTTON2 },
	{"forward button"		, MK_XBUTTON2 },
	{"button forward"		, MK_XBUTTON2 },
	{"mouse button forward"	, MK_XBUTTON2 },
	{"mouse forward"		, MK_XBUTTON2 },
};

inline const static std::map<int, std::string> MOUSE_BUTTONS_LABEL{
	{ MK_LBUTTON , "Left button" },
	{ MK_RBUTTON , "Right button" },
	{ MK_MBUTTON , "Middle button" },
	{ MK_XBUTTON1, "Back (X1) button" },
	{ MK_XBUTTON2, "Forward (X2) button" },
};


inline static bool IsLetterOrNumber(char c)
{
	c = std::tolower(c);
	return (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9');
}

inline static std::string GetKeyName(int key)
{
	if (key >= 0x30 && key <= 0x39)
	{
		return std::string() + static_cast<char>('0' + (key - 0x30));
	}
	if (key >= 0x41 && key <= 0x5A)
	{
		return std::string() + static_cast<char>('A' + (key - 0x41));
	}
	if (KEY_LABELS.contains(key))
	{
		return KEY_LABELS.at(key);
	}
	return "Unknown key";
}

inline static int ConvertLRKeys(int keycode)
{
	switch (keycode)
	{
	case VK_LSHIFT:
	case VK_RSHIFT:
		return VK_SHIFT;
	case VK_LMENU:
	case VK_RMENU:
		return VK_MENU;
	case VK_LCONTROL:
	case VK_RCONTROL:
		return VK_CONTROL;
	default:
		return keycode;
	}
}

inline static int GetKey(std::string keyName)
{
	if (!keyName.size())
	{
		return 0;
	}
	std::string keyNameOriginal = keyName;
	std::transform(keyName.begin(), keyName.end(), keyName.begin(), [](auto c) { return std::tolower(c); });
	if (keyName.starts_with("0x"))
	{
		return std::stol(keyName, nullptr, 0);
	}
	int keycode = 0;
	if (keyName.size() == 1)
	{
		if (keyName[0] >= '0' && keyName[0] <= '9')
		{
			keycode = KEY_MAP.at("0") + keyName[0] - '0';
		}
		if (keyName[0] >= 'a' && keyName[0] <= 'z')
		{
			keycode = KEY_MAP.at("a") + keyName[0] - 'a';
		}
	}
	if (!keycode && KEY_MAP.contains(keyName))
	{
		keycode = KEY_MAP.at(keyName);
	}
#ifdef _DEBUG
	if (ConvertLRKeys(keycode) != keycode)
	{
		MODUTILS_NAMESPACE::ULog::Get().dprintln("Warning: Left/right modifiers are not passed as wndproc wparam");
	}
#endif
	return keycode;
}

inline static int GetModifier(std::string modifierKeyName)
{
	if (modifierKeyName.empty())
	{
		return 0;
	}
	std::string keyNameOriginal = modifierKeyName;
	std::transform(modifierKeyName.begin(), modifierKeyName.end(), modifierKeyName.begin(), [](auto c) { return std::tolower(c); });
	if (modifierKeyName.starts_with("0x"))
	{
		return std::stol(modifierKeyName, nullptr, 0);
	}
	if (modifierKeyName.size() == 1)
	{
		if (modifierKeyName[0] >= '0' && modifierKeyName[0] <= '9')
		{
			return KEY_MAP.at("0") + modifierKeyName[0] - '0';
		}
		if (modifierKeyName[0] >= 'a' && modifierKeyName[0] <= 'z')
		{
			return KEY_MAP.at("a") + modifierKeyName[0] - 'a';
		}
	}
	if (KEY_MAP.contains(modifierKeyName))
	{
		return KEY_MAP.at(modifierKeyName);
	}
	return 0;
}

inline static int GetMouseButton(std::string btnName)
{
	std::transform(btnName.begin(), btnName.end(), btnName.begin(), [](auto c) { return std::tolower(c); });

	if (MOUSE_BUTTONS.contains(btnName))
	{
		return MOUSE_BUTTONS.at(btnName);
	}
	return 0;
}

inline static std::string GetMouseButtonName(int button)
{
	if (MOUSE_BUTTONS_LABEL.contains(button))
	{
		return MOUSE_BUTTONS_LABEL.at(button);
	}
	return "Unknown mouse button";
}

inline static int MouseButtonToVK(int mk)
{
	switch (mk)
	{
	case MK_LBUTTON: return VK_LBUTTON;
	case MK_RBUTTON: return VK_RBUTTON;
	case MK_MBUTTON: return VK_MBUTTON;
	case MK_XBUTTON1: return VK_XBUTTON1;
	case MK_XBUTTON2: return VK_XBUTTON2;
	}
	return 0;
}

template <bool emptyOrZero = true>
inline static bool AreModifiersHeld(std::vector<int> modifiers)
{
	std::vector<int> mods;
	std::copy_if(modifiers.begin(), modifiers.end(), std::back_inserter(mods), [](int mod) { return mod != 0; });
	if (mods.size() == 0)
	{
		return emptyOrZero;
	}
	bool out = true;
	for (int mod : mods)
	{
		SHORT state = GetAsyncKeyState(mod);
		bool pressed = state & ((1 << sizeof(SHORT) * CHAR_BIT) - 1);
		out &= pressed;
	}
	return out;
}


struct UHotKey
{
	bool IsMouseButton = false;
	int Key = 0;
	std::vector<int> Modifiers = { 0 };

	inline UHotKey(int Key = 0, bool isMouseButton = false, std::vector<int> Modifiers = std::vector<int>())
		: Key(Key), IsMouseButton(isMouseButton), Modifiers(Modifiers)
	{
	}

	inline UHotKey(std::string s)
	{
		Key = 0;
		Modifiers = std::vector<int>();

		std::string ls(s);
		// std::replace_if(
		// 	ls.begin(), ls.end(), [](unsigned char c) { return !IsLetterOrNumber(c) && std::string("+").find(c) == std::string::npos; }, ' ');
		auto isWhiteSpace = [](const char c)
			{
				constexpr const char* const whitespaces = " \t\n";
				return std::strchr(whitespaces, c) != nullptr;
			};
		auto trim = [&isWhiteSpace](const std::string &s)
			{
				std::string out = s;
				while (out.size() > 0 && isWhiteSpace(*out.begin()))
				{
					out.erase(out.begin());
				}
				while (out.size() > 0 && isWhiteSpace(*(out.end() - 1)))
				{
					out.erase(out.end() - 1);
				}
				return out;
			};

		std::istringstream ss(ls);
		std::vector<std::string> keys;
		std::string token;
		while (std::getline(ss, token, '+'))
		{
			keys.push_back(trim(token));
		}
		if (keys.empty())
		{
			return;
		}

		for (std::string modStr : std::vector(keys.begin(), keys.end() - 1))
		{
			MODUTILS_NAMESPACE::ULog::Get().dprintln(modStr);
			auto mod = GetModifier(modStr);
			if (mod)
			{
				Modifiers.push_back(mod);
			}
			else
			{
				MODUTILS_NAMESPACE::ULog::Get().eprintln("Unknown modifier: %s", modStr.c_str());
			}
		}
		std::string keystr = keys.back();
		MODUTILS_NAMESPACE::ULog::Get().dprintln(keystr);

		Key = GetMouseButton(keystr);
		if (Key)
		{
			IsMouseButton = true;
		}
		else
		{
			Key = ConvertLRKeys(GetKey(keystr));
		}

		if (!Key)
		{
			MODUTILS_NAMESPACE::ULog::Get().eprintln("Unknown key: %s", keystr.c_str());
		}

#if (_DEBUG)
		{
			std::string logstr;
			logstr += std::format("key: {}, modifiers: ", Key);
			for (int& mod : Modifiers)
			{
				logstr += std::format("{} ", mod);
			}
			MODUTILS_NAMESPACE::ULog::Get().dprintln(logstr);
		}
#endif
	}

	inline bool IsPressed(UINT wpMsg, WPARAM wParam, LPARAM lParam, bool repeat = true)
	{
		if (!Key)
		{
			return false;
		}

		if (IsMouseButton)
		{
			bool isBtnDown = wpMsg == WM_LBUTTONDOWN || wpMsg == WM_RBUTTONDOWN || wpMsg == WM_MBUTTONDOWN || wpMsg == WM_XBUTTONDOWN;
			return isBtnDown && LOWORD(wParam) == Key && AreModifiersHeld(Modifiers);
		}
		else
		{
			bool isKeydown = (wpMsg == WM_KEYDOWN || wpMsg == WM_SYSKEYDOWN);
			return isKeydown && wParam == Key && AreModifiersHeld(Modifiers) && (repeat ? true : ((lParam >> 30) & 1) == 0);
		}

	}

	inline bool IsPressedAsync()
	{
		if (!AreModifiersHeld(Modifiers))
		{
			return false;
		}
		int keycode = IsMouseButton ? MouseButtonToVK(Key) : Key;
		return (GetAsyncKeyState(keycode) & 1) != 0;
	}

	inline std::string ToString() const
	{
		std::stringstream ss;
		for (auto& mod : Modifiers)
		{
			ss << GetKeyName(mod) << " + ";
		}
		ss << GetKeyName(Key);
		return ss.str();
	}
};

KEYBOARDUTILS_NAMESPACE_END
#undef KEYBOARDUTILS_NAMESPACE_BEGIN
#undef KEYBOARDUTILS_NAMESPACE_END
